﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialEffect : MonoBehaviour {

    float alphaLevel = 100f;
    float animSpeed = 0.05f;

    Renderer rend;
	
	void Start () {
        rend = GetComponent<Renderer>();
        rend.material = new Material(rend.material);

	}

    void OnDestroy(){
        Destroy(rend.material);
    }
	
	
	void Update () {
        alphaLevel = Mathf.PingPong(Time.time * animSpeed, 0.3f);
        rend.material.color = new Color(rend.material.color.r, rend.material.color.g, rend.material.color.b, alphaLevel);
	}
}
