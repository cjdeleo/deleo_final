﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour {

    public Transform nextlevelp1;
    public Transform nextlevelp2;
    public Transform p1;
    public Transform p2;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            p1.position = nextlevelp1.position;
            p2.position = nextlevelp2.position;
        }

    }
}
