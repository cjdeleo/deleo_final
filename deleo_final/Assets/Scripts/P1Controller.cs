﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P1Controller : MonoBehaviour
{
    public float p1speed = 4.0f;
    public Text p1Score;
    private int p1score;
    public Transform p2;
    public Transform restartL2;
    public Transform restartL3;
    public Text textLabel;
    public Animator anim;
    public AudioSource gameMusic;
    public AudioSource winMusic;

    Vector3 movement;
    int floorMask;

    bool PickedUpCoin1 = false;
    bool PickedUpCoin2 = false;
    bool PickedUpCoin3 = false;
    bool PickedUpSpeedBoost = false;
    bool PickedUpRestartOpp = false;

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        p1score = 0;
        SetScoreText();
    }

    void FixedUpdate()
    {
        var x = Input.GetAxis("Horizontal_P1") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical_P1") * Time.deltaTime * p1speed;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKeyDown("c"))
        {
            transform.Translate(Vector3.up * 55 * Time.deltaTime, Space.World);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin Level 1"))
        {
            p1score += 100;
            p1speed = 4.0f;
            SetScoreText();
            PickedUpCoin1 = true;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("Speed Boost"))
        {
            p1speed += 3.0f;
            PickedUpSpeedBoost = true;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Coin Level 2"))
        {
            p1score += 500;
            p1speed = 4.0f;
            SetScoreText();
            PickedUpCoin2 = true;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("RestartOppL2"))
        {
            p2.position = restartL2.position;
            PickedUpRestartOpp = true;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("RestartOppL3"))
        {
            p2.position = restartL3.position;
            PickedUpRestartOpp = true;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Coin Level 3"))
        {
            p1score += 1000;
            p1speed = 4.0f;
            SetScoreText();
            PickedUpCoin3 = true;
            Destroy(other.gameObject);
            textLabel.text = "Player 1 Wins!!!\t\t\tPress R to Restart";
            anim.SetTrigger("GameOver");
            gameMusic.Stop();
            winMusic.Play();


        }
    }

    void SetScoreText(){
        p1Score.text = "P1 Score: " + p1score.ToString();
    }
}