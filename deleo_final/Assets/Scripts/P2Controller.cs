﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P2Controller : MonoBehaviour {

    public float p2speed = 4.0f;
    public Text p2Score;
    private int p2score;
    public Transform p1;
    public Transform restartL2;
    public Transform restartL3;
    public Text textLabel;
    public Animator anim;
    public AudioSource gameMusic;
    public AudioSource winMusic;

    Vector3 movement;
    int floorMask;


    bool PickedUpCoin1 = false;
    bool PickedUpCoin2 = false;
    bool PickedUpCoin3 = false;
    bool PickedUpSpeedBoost = false;
    bool PickedUpRestartOpp = false;

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        p2score = 0;
        SetScoreText ();
    }

    void FixedUpdate()
    {
        var x = Input.GetAxis("Horizontal_P2") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical_P2") * Time.deltaTime * p2speed;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKeyDown("n"))
        {
            transform.Translate(Vector3.up * 55 * Time.deltaTime, Space.World);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin Level 1")){
            p2score += 100;
            p2speed = 4.0f;
            SetScoreText ();
            PickedUpCoin1 = true;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Coin Level 2"))
        {
            p2score += 500;
            p2speed = 4.0f;
            SetScoreText();
            PickedUpCoin2 = true;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("Speed Boost"))
        {
            p2speed += 3.0f;
            PickedUpSpeedBoost = true;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("RestartOppL2"))
        {
            p1.position = restartL2.position;
            PickedUpRestartOpp = true;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("RestartOppL3"))
        {
            p1.position = restartL3.position;
            PickedUpRestartOpp = true;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Coin Level 3"))
        {
            p2score += 1000;
            p2speed = 4.0f;
            SetScoreText();
            PickedUpCoin3 = true;
            Destroy(other.gameObject);
            textLabel.text = "Player 2 Wins!!!\t\t\tPress R to Restart";
            anim.SetTrigger("GameOver");
            gameMusic.Stop();
            winMusic.Play();

        }
    }

    void SetScoreText(){
        p2Score.text = "P2 Score: " + p2score.ToString();
    }
}
